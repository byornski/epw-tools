# EPW TOOLS #

This repository is for tools/notes that I have found to perform an EPW calculation.


## General workflow

The general workflow involves running a ph.x calculation and then feeding those inputs into epw. 

### Phonon calculation
We first perform a ground state calculation 
- pw.x < scf.in > scf.out
- ph.x < ph.in > ph.out

### Moving files to the save directory
epw_prepare.py SEEDNAME

### Running epw
This needs unsymmetrised kpoints as inputs. The best way to generate these is with the kmesh.pl utility from QE. You can then run a non-selfconsistent run to generate the wvfns. These should be run in the same folder. 
pw.x < nscf.in > nscf.out
epw.x < epw.in > epw.out


## Parallelisation
### -nk/-npools
All the online documentation saying that pw.x and ph.x need to use the same parallelisation seem to be incorrect (at least on modern systems). You can use different npools on each at least for the phonon part. I'd guess that it's not true for the epw parts either. 

### -nimages with ph.x
If you do this, you have to move the correct files into the save folder from different _phX folders. The included epw_prepare.py script will do this for you. For future reference, these are:
- .phsave folder
- .dvscf1 files for each qpt (to .dvscf_qX)
- .dynX files to .dyn_qX