#!/usr/bin/env python3
#
# Post-processing script QE --> EPW 
# 10/09/2021 - Phonon postprocessing script
#
from pathlib import Path
import argparse
import re
import shutil

def copy(src: Path, dst: Path):
    shutil.copy(str(src), str(dst))  # str() only there for Python < (3, 6)

# Read prefix from arguments
parser = argparse.ArgumentParser(description='Convert ph.x output for epw')
parser.add_argument('prefix', type=str, help='Prefix used (eg MgB2)')
parser.add_argument('-s', '--save_folder', type=str, default="save", help="folder to put all phonon data in")
args = parser.parse_args()

# Shorthands for prefix and num_qpts
prefix = args.prefix
cwd = Path(".")

# Make the save folder
save = Path(args.save_folder)
try:
    save.mkdir(exist_ok=False)
except FileExistsError as exc:
    raise FileExistsError(f"Save folder \"{save}\" already exists!") from exc

print("Parsing _ph* folders for qpts")

# Find all _phX/prefix.q_Y folders
ph_dir = dict()
folder_glob = f"_ph*/{prefix}.q_*/"
folder_regex = re.compile(r"_ph\d+\/.+\.q_(\d+)") # Regex to parse q number
for f in cwd.glob(folder_glob):
    match = folder_regex.match(str(f))
    if match:
        iq = int(match.group(1))
        ph_dir[iq] = f
    else:
        raise Exception(f"Unparsable qpt file: {f}")

# Add iq=1 special case
ph_dir[1] = cwd / '_ph0'

# Count maximum phonons
maxq = max(ph_dir)

# Check qpts make a full set
for iq in range(1,max(ph_dir)+1):
    if iq not in ph_dir:
        raise Exception(f"Missing files for qpt {iq}")

print(f"Found {maxq} phonon qpts")

# Copy phsave folder from _ph0
print("Copying .phsave folder")
ph_save = f"{prefix}.phsave"
shutil.copytree(cwd / "_ph0" / ph_save, save / ph_save )

print("Copying qpts")
for iqpt in ph_dir:

    # Phonon save folder is the folder we found before
    ph_q_folder = ph_dir[iqpt]

    # Copy wavefunction dvscf file
    dvscf_file = ph_q_folder / f'{prefix}.dvscf1'
    dvscf_target = save      / f'{prefix}.dvscf_q{iqpt}'
    copy(dvscf_file, dvscf_target)

    # Copy dynamical matrix
    dyn_file   = cwd  / f'{prefix}.dyn{iqpt}'
    dyn_target = save / f'{prefix}.dyn_q{iqpt}'
    copy(dyn_file, dyn_target)

print("Finished! You can now remove the _ph folders to save space.")
